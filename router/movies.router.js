const Koa = require('koa');
const Router = require('@koa/router');
const { getMovie, getAllMovies, searchAndReplace } = require('../controllers/movie.controllers');
const { fieldValidation } = require('../Middleware/movie.middleware');
const schema = require('../models/movie.joi');


const app = new Koa();
const router = new Router({
  prefix: '/api'
});

//API Routes 
router.get('/getMovie/:search',
    fieldValidation(schema.searchSchema,'params'), 
    fieldValidation(schema.yearSchema,'header'),
    getMovie
);
router.get('/getAllMovies',
    fieldValidation(schema.pagesSchema,'header'),
    getAllMovies
);
router.post('/searchAndReplace',
    fieldValidation(schema.searchAndReplaceSchema,'body'),
    searchAndReplace
);

app.use(router.routes());

module.exports = router;