const axios = require('axios');

//This function looks for the information of the movie consulting through the OMDAPI API.
const api_omdbapi = async (search,year) => {

    try {
        if(year){
            //Search for a film by title and year
            resultAPI = await axios.get(process.env.API + '&t=' + search + '&y=' + year);
            const { data } = resultAPI;
            return data; 
        }
        else{
            //Search for a film by title
            resultAPI = await axios.get(process.env.API + '&t=' + search);
            const { data } = resultAPI;

            return data; 
        }

    } catch (error) {
        console.error(error);
    }
}
module.exports = api_omdbapi; 

