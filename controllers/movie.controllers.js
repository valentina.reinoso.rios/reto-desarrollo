const api_omdbapi = require('./api/api_omdbapi');
const Movie = require('../models/movie.models');
 
//This function allows to obtain the information of a movie using the title and optionally the year.
const getMovie = async(ctx) => {        
    const { search } = ctx.params;
    const { year } = ctx.header;

    let result = {};
    let resultSearch = {};

    //This function looks for the information of the movie consulting through the OMDAPI API
    await api_omdbapi(search,year)
        .then(resp => {result = resp})

    try{    
        const { Response, Error, Title, Year, Released, Genre, Director, Actors, Plot, Ratings } = result;

        console.log(result);
        if(Response == 'False'){
            ctx.status = 404;
            ctx.body = {
                Error
            }
        }else{
            const resultSearch = await Movie.findOne({Title});

            if(resultSearch){
                const { Title, Year, Released, Genre, Director, Actors, Plot, Ratings } = resultSearch;
                
                ctx.body = {
                    Title, 
                    Year, 
                    Released, 
                    Genre, 
                    Director, 
                    Actors, 
                    Plot, 
                    Ratings
                };
            }
            else{
                const newMovie = new Movie({Title, Year, Released, Genre, Director, Actors, Plot, Ratings});
                await newMovie.save();

                ctx.body = {
                    Title, 
                    Year, 
                    Released, 
                    Genre, 
                    Director, 
                    Actors, 
                    Plot, 
                    Ratings
                };
           
            }   
        }        
    } catch (error) {
        let Error = 'Failed to insert a value into the database.';
        
        ctx.status = 400;
        ctx.body = {
            Error
        }
    }
    
}

//This function allows to bring the information of the movies stored in the database in groups of 5.
//For this, the page number is needed in the header.
const getAllMovies = async(ctx) => {  
    const { pages } = ctx.header
    const limite = 5;
    const desde = limite * (Number(pages)-1)
    const countMovies = await Movie.countDocuments();
    if( countMovies < pages)
        if (countMovies > 5){
            const allMovies = await Movie.find()
                .limit(limite)
                .skip(desde);

            ctx.body = {
                allMovies
            };
        }else{
            const allMovies = await Movie.find()
                .limit(5);

            ctx.body = {
                allMovies
            };
        }
    else{
        ctx.status = 400;
        ctx.body = {
            Error: 'Not movies in this page.'
        }
    }
}


//This function has 3 parameters "movie" "find" and "replace". 
//Finds the movie mentioned in "movie" and then replaces all references to the variable "find" in the plot attribute of the Movie object 
//with the variable "replace" and saves them in the database
const searchAndReplace = async(ctx) => {  
    const { movie, find, replace } = ctx.request.body;

    const movieSearch = await Movie.findOne({Title: movie}).exec();

    if(movieSearch){
        const { Plot } = movieSearch;
        var newPlot = Plot.replaceAll(find, replace);
        let result = {};

        await Movie.findOneAndUpdate({Title: movie},{ Plot: newPlot}).exec();

        ctx.body = {
            newPlot
        };
    }else 
    {
        let Err = 'Movie not found!';
        ctx.status = 404
        ctx.body = {
            Err
        }
    }
}
module.exports = { getMovie, getAllMovies, searchAndReplace};