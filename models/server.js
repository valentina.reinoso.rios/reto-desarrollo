const serve = require('koa-static');
const Koa = require('koa');
const Router = require('@koa/router');
const bodyParser = require('koa-bodyparser');
const dbConnection = require('../database/config');
const routerMovie = require('../router/movies.router');

class Server {
    constructor() {
        this.app = new Koa();
        this.router = new Router();
    
        this.port = process.env.PORT;
        
        //Database connection 
        this.connectDB();

        //Middleware
        this.middleware();

        //Routes
        this.routes(); 
    }

    //Database connection function
    async connectDB() {
        await dbConnection();
    }
    
    //Middleware function
    middleware() {
        this.app.use(bodyParser());
    }

    //Routes function
    routes() {
        this.app.use(routerMovie.routes()).use(routerMovie.allowedMethods());  
    }
    
    //Port listener function
    listen(){
        this.app.listen(this.port, () => {
            console.log('Listening on port', this.port);
        });
    }
}

module.exports = Server;