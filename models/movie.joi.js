const Joi = require('joi');

const schema = {
    //EndPoint getMovie()
    searchSchema: Joi.object({
        search: Joi.string()
            .min(3)
            .max(30)
            .required()
    }),
    yearSchema: Joi.object({
        year: Joi.number()
            .min(1200)
            .max(2021)
    }),

    //EndPoint getAllMovie()
    pagesSchema: Joi.object({
        pages: Joi.number()
            .min(1)
            .max(20)
    }),

    //EndPoint searchAndReplace()
    searchAndReplaceSchema: Joi.object({
        movie: Joi.string()
            .min(3)
            .max(30)
            .required(),
        find: Joi.string()
            .min(1)
            .max(20)
            .required(),
        replace: Joi.string()
            .min(1)
            .max(20)
            .required()
    })
}

module.exports = schema;


