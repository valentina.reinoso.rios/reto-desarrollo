const { Schema, model } = require('mongoose');

const MovieSchema = Schema ({
    Title: {
       type: String,
       require: [true, 'The title is required'],
       Unique: true
    },
    Year: {
        type: Number,
        require: [true, 'The year is required'] 
    },
    Released: {
        type: Date,
        require: [true, 'The released is required'] 
    },
    Genre: {
        type: [String],
        require: [true, 'The genre is required'] 
    },
    Director: {
        type: String,
        require: [true, 'The director is required'] 
    },
    Actors: {
        type: [String],
        require: [true, 'The actors is required'] 
    },
    Plot: {
        type: String,
        require: [true, 'The plot is required'] 
    },
    Ratings: {
        type: [{ Source: String,
                 Value: String }],
        require: [true, 'The ratings is required'] 
    }
        
});


module.exports = model('Movie', MovieSchema);

