const fieldValidation = (schema,property) => {
    return async(ctx, next) => { 
        let object = {}
        let SchemaValidate

        property == 'header'?object = ctx.header:
        property == 'params'?object = ctx.params:
        property == 'query'?object = ctx.query:
        property == 'body'?object = ctx.request.body:
        console.log('No existe Propiedad para validar campos');
     
        if(property == 'header'){
            SchemaValidate = schema.validate(object, { allowUnknown: true });
        } else {
            SchemaValidate = schema.validate(object);
        }

        const { error } = SchemaValidate
        const valid = error == undefined; 

        if (valid){
            await next()
        }else {
            const { details } = error
            const message = details.map(i => i.message).join(',');
            //logger.log({"level": "error", "message": `*${ new Date().toJSON() } *validator.js *validaCampos *request: schema: ${ schema } *response: ${ message }`})
            ctx.status = 422
            ctx.body =  { 
                error: message 
            }
        }
    }
}

module.exports = {
    fieldValidation
}